var path = require('path');

module.exports = {
  mode: 'none',

  entry: [
    path.resolve(__dirname, "./main.js")
  ],

  output: {
    path: path.resolve(__dirname, "../server/webapp"),
    filename: "bundle.js"    
  },

  module: {

    rules: [
      /* javascript */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react']
          }
        }
      },

      /* jsx */
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react']
          }
        }
      }
    ]

  },

  /* our './myComponent' requires or imports are equivalent to 'myComponent.jsx' */
  resolve: {
    extensions: ['*', '.js', '.jsx']
  }
}
