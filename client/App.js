import React from "react";

class App extends React.Component {

  constructor() {
    super();

    this.timer;
    this.state = { 'data': {} };
  }

  getData() {
    let data = {};
    let xhr = new XMLHttpRequest();

    xhr.open('GET', encodeURI('/kpis'));
    xhr.responseType = 'json';
    /*
        xhr.onload = function () {
          if (xhr.status === 200) { data = xhr.response; }
          this.setState({ 'data': data });
        }.bind(this);
    */
    xhr.onload = () => {
      if (xhr.status === 200) { data = xhr.response; }
      this.setState({ 'data': data });
    };

    xhr.send();
  }

  componentDidMount() {
    //this.timer = setInterval(this.getData.bind(this), 5000);
    this.timer = setInterval(() => this.getData(), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    let tests = [];
    if (this.state.data.metrics) {
      for (let i = 0; i < this.state.data.metrics.length; i++) {
        let style = { 'color': 'green' };
        let count = this.state.data.metrics[i].metrics.totalCount;

        if (this.state.data.metrics[i].metrics.failCount > 0) {
          style = { 'color': 'red' };
          count = this.state.data.metrics[i].metrics.failCount;
        }

        tests.push(<div key={i} style={style}>
          <div style={{ fontSize: "3em" }}>{this.state.data.metrics[i].project}</div>
          <div style={{ fontSize: "2em" }}>{this.state.data.metrics[i].job}</div>
          <div style={{ fontSize: "3em" }}>{count}</div>
        </div>);
      }
    }

    let builds = [];
    if (this.state.data.builds) {
      for (let i = 0; i < this.state.data.builds.length; i++) {
        for (let j = 0; j < this.state.data.builds[i].builds.length; j++) {
          let style;
          switch (this.state.data.builds[i].builds[j].status) {
            case 'success':
              style = { 'color': 'green' };
              break;

            case 'error':
              style = { 'color': 'red' };
              break;

            case 'running':
              style = { 'color': 'black' };
              break;

            default:
          }
          
          builds.push(<div key={'bc'.concat(i)} style={style}>
            <div style={{ fontSize: "3em" }}>{this.state.data.builds[i].project}</div>
            <div style={{ fontSize: "2em" }}>
              {this.state.data.builds[i].type} : {this.state.data.builds[i].builds[j].view}
            </div>
            <div style={{ fontSize: "1em" }}>({this.state.data.builds[i].builds[j].builds.join()})</div>
          </div>);
        }
      }
    }

    return (
      <div>
        <div style={{ fontSize: "6em" }}>
          <center>Jenkins Wallboard</center>
        </div>

        <div>
          <div style={{ fontSize: "4em" }}>TESTS</div>
          {tests}
        </div>

        <div>
          <div style={{ fontSize: "4em" }}>BUILDS</div>
          {builds}
        </div>

      </div>
    );
  }
}

export default App;
