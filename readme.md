# Simple React Wallboard for Jenkins engined by a Node Server
A long time ago, for supervision purpose, I needed to display, among others, some real time KPIs retrieved from our Jenkins IC Platform on a big screen in our team's open-space.  
After several weeks of research, I have not found a suitable solution. So I decided to code my own tool.  
This project shows how I managed to do it with React/Node and the Jenkins API.

For more details, I suggest you to read the following post on my blog : [part 1][0] and [part 2][1].

# Architecture
To avoid XSS problem, the node server is a proxy for our Jenkins server (other domain).  
Its role is the following :

+ rendering the React application
+ proxying the Jenkins server by offering a REST API which is redirected on the Jenkins REST API

```mermaid
graph TD;
  ReactClient-->ProxyRESTApi;
  ProxyRESTApi-->NodeProxyServer;
  NodeProxyServer-->JenkinsRESTApi;
  JenkinsRESTApi-->JenkinsServer;
```

# Installation

1. clone or dowload the project
2. in `client` directory
  * do `npm i`
  * run `webpack`
3. in `jenkins` directory
  * do `npm i`
  * run `node jenkins`
4. in `server` directory
  * do `npm i`
  * run `node server`

# Test
In your browser, enter `http://localhost:3001` (if you run node server on its default port). You shoukd see the following result:  

![screenshot](/screenshot.png)

[0]:https://entwicklerfr.gitlab.io/2018/04/react---jenkins-wallboard-1/2/
[1]:https://entwicklerfr.gitlab.io/2018/04/react---jenkins-wallboard-2/2/

