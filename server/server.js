'use strict';

/* globals */
global.__BASE = __dirname;

/* we will use path.join for our requires to be posix and windows compatible */
var path = require('path');

/* usage verification */
var { Usage } = require(path.join((__BASE + '/src/Usage')));
var myUsage = new Usage();
myUsage.check();

/* object which retrieves jenkins kpis */
var { Kpi } = require(path.join((__BASE + '/src/Kpi')));
var myKpi = new Kpi();

/* run the cache update every 15 seconds */
function refreshCache() {
	myKpi.refresh();
}
setInterval(refreshCache, 15000);

/* */
var express = require('express');
var app = express();

/* entry point for static files of the REACT app */
/* so a request on http://host:port will return the REACT app */
app.use(express.static(path.join(__BASE, 'webapp')));

/* get jenkins kpis */
app.get('/kpis', function (req, res) {
	res.status(200).json(myKpi);
	return;
});

/* */
app.listen(myUsage.proxyPort, function () {
	console.log('Proxy server started on port [%d]', myUsage.proxyPort);
});
