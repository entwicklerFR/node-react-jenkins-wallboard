'use strict';

/* */
module.exports = {

	'proxyPort': '3001',

	'urlJenkinsMetricsList': [
		{ 'project': 'BigProject', 'job': 'BigProject-Metrics', 'url': 'http://localhost:3999/jenkins/metrics/bigproject' },
		{ 'project': 'SmallProject', 'job': 'SmallProject-Metrics', 'url': 'http://localhost:3999/jenkins/metrics/smallproject' }
	],

	'urlJenkinsBuildsList': [
		{ 'project': 'BigProject', 'type': 'BUILD', 'url': 'http://localhost:3999/jenkins/builds/bigproject' },
		{ 'project': 'SmallProject', 'type': 'BUILD', 'url': 'http://localhost:3999/jenkins/builds/smallproject' }
	]
	
}