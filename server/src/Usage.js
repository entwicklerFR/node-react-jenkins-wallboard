'use strict';

/* we will use path.join for our requires to be posix and windows compatible */
var path = require('path');

/* get config settings */
var myConfig = require(path.join((__BASE + '/src/config')));

/* */
class Usage {

  /* */
  constructor() {
    this._argv;
    this._proxyPort;
    this._initProps();
  }

  /* */
  _initProps() {
    this._argv = process.argv;

    for (let i = 2; i < this._argv.length; i++) {
      //console.log('argv[%i] : [%s]', i, this._argv[i]);
      if (this._argv[i] === '--p' || this._argv[i] === '--port') {
        this._proxyPort = this._argv[i + 1];
      }
    }

  }

  /* */
  check() {
    for (let i = 2; i < this._argv.length; i++) {
      if (this._argv[i] === '--h' || this._argv[i] === "--help") {
        console.log('');        
        console.log('Usage :');
        console.log('-------');
        console.log('');
        console.log('--h (--help)                :  displays this help');
        console.log('--p (--port) <port number>  :  sets proxy port number (if not : config file value or 3000 by default)');
        console.log('');
        console.log('Usage examples : start server --help');
        console.log('               : start server --p 3000');
        process.exit(0);
      }
    }

    console.log('Get more info with "node server --h"');
  }

  /* */
  get proxyPort() {
    if (this._proxyPort) { return this._proxyPort; }
    if (myConfig.proxyPort) { return myConfig.proxyPort; }
    return 3000;
  }

}

/* */
exports.Usage = Usage;