'use strict';

/* */
var request = require('request-promise');

/* we will use path.join for our requires to be posix and windows compatible */
var path = require('path');

/* get config settings */
var myConfig = require(path.join((__BASE + '/src/config')));

/* */
class Kpi {

  /* */
  constructor() {
    this.metrics; // tests kpis
    this.builds; // builds kpis

    this._initProps();
  }

  /* */
  _initProps(pMoment) {
    this.metrics = [];
    this.builds = [];
  }

  /* the jenkins anonymous account must have the read role */
  async refresh() {
    let metricsByProject = [];
    let buildsByProject = [];

    /* metrics */
    let metricsRequests = myConfig.urlJenkinsMetricsList.map(async function (mapRow) {
      let options = { url: mapRow.url };
      let body;

      try {
        body = await request.get(options);
      } catch (error) {
        console.error(error);
        return;
      }

      let json = JSON.parse(body);

      // The last build is builds[0]
      let actions = json.builds[0].actions;
      let metrics = {};
      for (let i = 0; i < actions.length; i++) {
        if (actions[i].totalCount) {
          metrics.failCount = actions[i].failCount;
          metrics.skipCount = actions[i].skipCount;
          metrics.totalCount = actions[i].totalCount;
        }
      }

      metricsByProject.push({ 'project': mapRow.project, 'job': mapRow.job, 'metrics': metrics });
    });

    try {
      await Promise.all(metricsRequests);
    } catch (error) {
      console.error(error);
      return;
    }

    this.metrics = metricsByProject.sort(function (metricA, metricB) {
      let projectA = metricA.project.toLowerCase();
      let projectB = metricB.project.toLowerCase();
      if (projectA < projectB) { return -1; }
      if (projectA > projectB) { return 1; }
      return 0;
    });

    /* builds */
    let buildsRequests = myConfig.urlJenkinsBuildsList.map(async function (mapRow) {
      let options = { url: mapRow.url };
      let body;

      try {
        body = await request.get(options);
      } catch (error) {
        console.error(error);
        return;
      }

      let json = JSON.parse(body);

      let views = json.views;
      let builds = [];
      let status;

      // we analyse all the views
      for (let i = 0; i < views.length; i++) {
        let error = false;
        let running = false;
        let buildsSuccess = [];
        let buildsError = [];
        let buildsRunning = [];
        let buildsList = [];

        // Builds in error ?
        for (let j = 0; j < views[i].jobs.length; j++) {

          if (views[i].jobs[j].color) {

            if (views[i].jobs[j].color === 'blue') {
              buildsSuccess.push(views[i].jobs[j].url);
            }

            if (views[i].jobs[j].color === 'red') {
              error = true;
              buildsError.push(views[i].jobs[j].url);
            }

            if (views[i].jobs[j].color.indexOf("anime") != -1) {
              running = true;
              buildsRunning.push(views[i].jobs[j].url);
            }
          }
        }

        // by priority order
        // 1 - running
        // 2 - error
        // 3 - success
        if (!error && !running) {
          status = 'success';
          buildsList = buildsSuccess;
        }
        if (error) {
          status = 'error';
          buildsList = buildsError;
        }
        if (running) {
          status = 'running';
          buildsList = buildsRunning;
        }

        builds.push({ 'view': views[i].name, 'status': status, 'builds': buildsList });
      }

      buildsByProject.push({ 'project': mapRow.project, 'type': mapRow.type, 'builds': builds });

    });

    try {
      await Promise.all(buildsRequests);
    } catch (error) {
      console.error(error);
      return;
    }

    this.builds = buildsByProject.sort(function (buildA, buildB) {
      let projectA = buildA.project.toLowerCase() + buildA.type.toLowerCase();
      let projectB = buildB.project.toLowerCase() + buildB.type.toLowerCase();;
      if (projectA < projectB) { return -1; }
      if (projectA > projectB) { return 1; }
      return 0;
    });
  }

}

exports.Kpi = Kpi;