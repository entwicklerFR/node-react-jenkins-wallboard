'use strict';

/* */
var express = require('express');
var app = express();

var buildsForBigProject =
    {
        "_class": "com.Instancebees.hudson.plugins.folder.Folder",
        "views": [
            {
                "_class": "hudson.model.AllView",
                "jobs": [
                    {
                        "_class": "com.github.mjdetullio.jenkins.plugins.multibranch.FreeStyleMultiBranchProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDeploy/"
                    },
                    {
                        "_class": "com.github.mjdetullio.jenkins.plugins.multibranch.FreeStyleMultiBranchProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDispatch/"
                    },
                    {
                        "_class": "com.github.mjdetullio.jenkins.plugins.multibranch.FreeStyleMultiBranchProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceMemory/"
                    }
                ],
                "name": "All"
            },
            {
                "_class": "hudson.model.ListView",
                "jobs": [
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDeploy/job/version2_release/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDispatch/job/version2_release/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceMemory/job/version2_release/",
                        "color": "blue"
                    }
                ],
                "name": "version1"
            },
            {
                "_class": "hudson.model.ListView",
                "jobs": [
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDeploy/job/version2_release/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDispatch/job/version2_release/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceMemory/job/version2_release/",
                        "color": "red"
                    }
                ],
                "name": "version2"
            },
            {
                "_class": "hudson.model.ListView",
                "jobs": [
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDeploy/job/version3_release/",
                        "color": "blue_anime"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceDispatch/job/version3_release/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.model.FreeStyleProject",
                        "url": "http://my.jenkins.domain.url/job/BigProject/job/BUILD/job/InstanceMemory/job/version3_release/",
                        "color": "blue"
                    }
                ],
                "name": "version3"
            }
        ]
    }

var buildsForSmallProject =
    {
        "_class": "com.cloudbees.hudson.plugins.folder.Folder",
        "views": [
            {
                "_class": "hudson.model.AllView",
                "jobs": [
                    {
                        "_class": "hudson.maven.MavenModuleSet",
                        "url": "http:/my.jenkins.domain.url/job/SmallProject/job/SmallProjectWS/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.maven.MavenModuleSet",
                        "url": "http:/my.jenkins.domain.url/job/SmallProject/job/SmallProjectEngine/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.maven.MavenModuleSet",
                        "url": "http:/my.jenkins.domain.url/job/SmallProject/job/SmallProjectData/",
                        "color": "blue"
                    },
                    {
                        "_class": "hudson.maven.MavenModuleSet",
                        "url": "http:/my.jenkins.domain.url/job/SmallProject/job/SmallProjectDispatcher/",
                        "color": "blue"
                    }
                ],
                "name": "All"
            }
        ]
    }

var metricsForBigProject =
    {
        "_class": "hudson.model.FreeStyleProject",
        "builds": [
            {
                "_class": "hudson.model.FreeStyleBuild",
                "actions": [
                    {
                        "_class": "hudson.model.CauseAction"
                    },
                    {
                        "_class": "hudson.tasks.junit.TestResultAction",
                        "failCount": 0,
                        "skipCount": 8,
                        "totalCount": 2590
                    }
                ],
                "number": 14356
            },
            {
                "_class": "hudson.model.FreeStyleBuild",
                "actions": [
                    {
                        "_class": "hudson.model.CauseAction"
                    },
                    {
                        "_class": "hudson.tasks.junit.TestResultAction",
                        "failCount": 0,
                        "skipCount": 8,
                        "totalCount": 2590
                    }
                ],
                "number": 14355
            },
            {
                "_class": "hudson.model.FreeStyleBuild",
                "actions": [
                    {
                        "_class": "hudson.model.CauseAction"
                    },
                    {
                        "_class": "hudson.tasks.junit.TestResultAction",
                        "failCount": 0,
                        "skipCount": 8,
                        "totalCount": 2590
                    }
                ],
                "number": 14354
            }
        ]
    }

var metricsForSmallProject =
    {
        "_class": "hudson.model.FreeStyleProject",
        "builds": [
            {
                "_class": "hudson.model.FreeStyleBuild",
                "actions": [
                    {
                        "_class": "hudson.model.CauseAction"
                    },
                    {
                        "_class": "hudson.tasks.junit.TestResultAction",
                        "failCount": 4,
                        "skipCount": 1,
                        "totalCount": 5545
                    }
                ],
                "number": 2222
            },
            {
                "_class": "hudson.model.FreeStyleBuild",
                "actions": [
                    {
                        "_class": "hudson.model.CauseAction"
                    },
                    {
                        "_class": "hudson.tasks.junit.TestResultAction",
                        "failCount": 0,
                        "skipCount": 1,
                        "totalCount": 5545
                    }
                ],
                "number": 2221
            }
        ]
    }



app.get('/jenkins/builds/:projectname', function (req, res) {
    console.log("the jenkins url to get the list of builds should look like => ");
    console.log("http://my.jenkins.domain.url/job/my_project_name/api/json?pretty=true&tree=views[name,jobs[url,color]]")

    if (req.params.projectname === 'bigproject') {
        res.status(200).json(buildsForBigProject);
        return;
    }

    res.status(200).json(buildsForSmallProject);
    return;
});

app.get('/jenkins/metrics/:projectname', function (req, res) {
    console.log("the jenkins url to get the list of metrics (test results for instance) should look like => ");
    console.log("http://my.jenkins.domain.url/job/my_project_name/job/my_job_name/api/json?&pretty=true&tree=builds[number,actions[failCount,skipCount,totalCount]]")
    if (req.params.projectname === 'bigproject') {
        res.status(200).json(metricsForBigProject);
        return;
    }

    res.status(200).json(metricsForSmallProject);
    return;
});

/* */
app.listen(3999, function () {
    console.log('Fake Jenkins server started on port [%d]', 3999);
});